--liquibase formatted sql

--changeset erubinic:1 logicalFilePath:00000700-insert-rp-first.sql splitStatements:true

INSERT INTO results.rp_before_finals (student_id, activity, dz_2, dz_4, dz_5, dz_6) VALUES
(3, 8, 5, 5, 5, 2),
(4, 9, 5, 5, 5, 5),
(5, 7, 5, 5, 4, 4),
(6, 7, 5, 5, 3, 2),
(7, 5, 5, 5, 5, 4),
(10, 10, 5, 5, 5, 5),
(12, 10, 5, 5, 5, 3),
(13, 7, 5, 5, 5, 0),
(18, 7, 5, 2, 0, 2),
(20, 10, 5, 5, 0, 4),
(24, 10, 5, 5, 0, 4),
(25, 8, 5, 0, 4, 4),
(26, 10, 5, 5, 2, 0);

INSERT INTO results.rp_finals (student_id, pres_content, pres_live, report, bitbucket, quality) VALUES
(3, 16, 14, 16, 15, 12),
(4, 20, 20, 18, 18, 19),
(5, 12, 14, 17, 10, 14),
(6, 20, 16, 20, 0, 14),
(7, 14, 14, 20, 18, 16),
(10, 20, 20, 20, 15, 20),
(12, 20, 20, 18, 18, 19),
(13, 18, 18, 0, 13, 17),
(18, 16, 14, 16, 0, 13),
(20, 20, 20, 20, 20, 17),
(24, 20, 20, 20, 20, 17),
(25, 20, 20, 18, 18, 19),
(26, 12, 12, 16, 18, 12);






