--liquibase formatted sql

--changeset erubinic:1 logicalFilePath:00000401-add-remaining-itd-s2.sql

UPDATE results.itd_second_seminar
SET pres_design = 8, pres_content = 8, pres_live = 7, quality = 17
WHERE student_id=1;

UPDATE results.itd_second_seminar
SET pres_design = 10, pres_content = 10, pres_live = 9, quality = 14
WHERE student_id=9;