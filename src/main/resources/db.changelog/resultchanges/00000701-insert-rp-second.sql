--liquibase formatted sql

--changeset erubinic:1 logicalFilePath:00000701-insert-rp-second.sql splitStatements:true

INSERT INTO results.rp_before_finals (student_id, activity, dz_2, dz_4, dz_5, dz_6) VALUES
(1, 6, 0, 5, 4, 0),
(2, 3, 3, 5, 2, 2),
(8, 2, 5, 5, 0, 0),
(9, 5, 5, 5, 0, 0),
(11, 3, 3, 0, 0, 0),
(14, 6, 2, 5, 0, 0),
(15, 3, 5, 0, 0, 0),
(16, 2, 5, 0, 0, 0),
(17, 6, 2, 5, 4, 2),
(19, 3, 0, 0, 0, 0),
(21, 4, 0, 2, 0, 0),
(22, 9, 0, 5, 0, 2),
(23, 8, 5, 5, 0, 0);







