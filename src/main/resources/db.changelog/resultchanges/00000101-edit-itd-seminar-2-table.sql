--liquibase formatted sql

--changeset erubinic:1 logicalFilePath:00000101-edit-itd-seminar-2-table.sql

EXEC sp_rename 'results.itdSecondSeminar', 'itd_second_seminar';

ALTER TABLE results.itd_second_seminar
DROP COLUMN quantity, formatting;

ALTER TABLE results.itd_second_seminar
ADD negative_points INTEGER;
