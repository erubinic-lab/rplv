--liquibase formatted sql

--changeset erubinic:1 logicalFilePath:00000102-edit-itd-activity-table.sql

EXEC sp_rename 'results.itd_activity.student_idd', 'student_id', 'COLUMN';
