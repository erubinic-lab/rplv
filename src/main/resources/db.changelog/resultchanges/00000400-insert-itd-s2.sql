--liquibase formatted sql

--changeset erubinic:1 logicalFilePath:00000400-insert-itd-s2.sql

INSERT INTO results.itd_second_seminar (student_id, pres_design, pres_content, pres_live, quality, negative_points) VALUES
(1, NULL, NULL, NULL, NULL, NULL),
(2, 7, 5, 5, 8, NULL),
(3, 7, 5, 6, 9, NULL),
(4, 8, 6, 8, 15, NULL),
(5, 6, 6, 8, 13, NULL),
(6, 10, 8, 7, 11, 1),
(7, 7, 10, 9, 16, 2),
(8, 7, 9, 10, 12, NULL),
(9, NULL, NULL, NULL, NULL, NULL),
(10, 6, 10, 10, 20, 3),
(11, 7, 5, 5, 6, 6),
(12, 8, 8, 10, 17, 1),
(13, 6, 5, 8, 15, NULL),
(14, 10, 10, 10, 20, NULL),
(15, 6, 5, 5, 14, NULL),
(16, 8, 8, 7, 16, 2),
(17, 9, 8, 8, 18, 2),
(18, 10, 10, 7, 18, NULL),
(19, 10, 9, 7, 17, 1),
(20, 10, 8, 10, 20, NULL),
(21, 5, 8, 5, 19, NULL),
(22, 7, 6, 6, 20, NULL),
(23, 7, 7, 8, 16, NULL),
(24, 8, 10, 9, 20, 1),
(25, 9, 9, 10, 19, 2),
(26, 6, 8, 9, 14, NULL);





