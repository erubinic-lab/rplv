--liquibase formatted sql

--changeset erubinic:1 logicalFilePath:00000600-create-rp-results-tables.sql splitStatements:true

CREATE TABLE results.rp_before_finals (
    id INTEGER IDENTITY(1,1) PRIMARY KEY,
    student_id INTEGER NOT NULL FOREIGN KEY REFERENCES results.student(id),
    activity INTEGER,
    dz_2 INTEGER,
    dz_4 INTEGER,
    dz_5 INTEGER,
    dz_6 INTEGER
);

CREATE TABLE results.rp_finals (
    id INTEGER IDENTITY(1,1) PRIMARY KEY,
    student_id INTEGER NOT NULL FOREIGN KEY REFERENCES results.student(id),
    pres_content INTEGER,
    pres_live INTEGER,
    report INTEGER,
    bitbucket INTEGER,
    quality INTEGER
);




