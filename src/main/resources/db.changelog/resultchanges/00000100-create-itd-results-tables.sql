--liquibase formatted sql

--changeset erubinic:1 logicalFilePath:00000100-create-itd-results-tables.sql splitStatements:true

CREATE SCHEMA results;

CREATE TABLE results.student (
    id INTEGER PRIMARY KEY,
    jmbag VARCHAR(9) NOT NULL
);

CREATE TABLE results.itd_first_seminar (
    id INTEGER IDENTITY(1,1) PRIMARY KEY,
    student_id INTEGER NOT NULL FOREIGN KEY REFERENCES results.student(id),
    pres_design INTEGER,
    pres_content INTEGER,
    pres_live INTEGER,
    quantity INTEGER,
    formatting INTEGER,
    quality INTEGER
);

CREATE TABLE results.itdSecondSeminar (
    id INTEGER IDENTITY(1,1) PRIMARY KEY,
    student_id INTEGER NOT NULL FOREIGN KEY REFERENCES results.student(id),
    pres_design INTEGER,
    pres_content INTEGER,
    pres_live INTEGER,
    quantity INTEGER,
    formatting INTEGER,
    quality INTEGER
);

CREATE TABLE results.itd_activity (
    id INTEGER IDENTITY(1,1) PRIMARY KEY,
    student_idd INTEGER NOT NULL FOREIGN KEY REFERENCES results.student(id),
    activity INTEGER NOT NULL
);
