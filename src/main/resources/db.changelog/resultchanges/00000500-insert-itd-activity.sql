--liquibase formatted sql

--changeset erubinic:1 logicalFilePath:00000500-insert-itd-activity.sql

INSERT INTO results.itd_activity (student_id, activity) VALUES
(1, 7),
(2, 5),
(3, 5),
(4, 8),
(5, 6),
(6, 9),
(7, 5),
(8, 4),
(9, 7),
(10, 10),
(11, 5),
(12, 10),
(13, 10),
(14, 7),
(15, 4),
(16, 6),
(17, 6),
(18, 10),
(19, 6),
(20, 10),
(21, 5),
(22, 10),
(23, 8),
(24, 10),
(25, 6),
(26, 6);






