package lv03.gridbaglayout;

import java.awt.*;

import javax.swing.*;


public class HelloGridBag extends JFrame {
	
	private GridPanel gridPanel;
	
	public HelloGridBag() {
		super("Hello Grid Bag layout");
		
		setLayout(new BorderLayout());

		gridPanel = new GridPanel();

		add(gridPanel, BorderLayout.CENTER);

		setSize(500, 500);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> new HelloGridBag());
	}
}
