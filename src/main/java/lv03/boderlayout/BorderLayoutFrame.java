package lv03.boderlayout;


import javax.swing.*;
import java.awt.*;

public class BorderLayoutFrame extends JFrame {
    private JButton buttonPageStart;
    private JButton buttonPageEnd;
    private JButton buttonLineStart;
    private JButton buttonLineEnd;
    private JTextArea textAreaCenter;

    public BorderLayoutFrame() {
        super("Hello Border Layout");

        setLayout(new BorderLayout());

        textAreaCenter = new JTextArea();
        buttonPageStart = new JButton("Page Start");
        buttonPageStart.setPreferredSize(new Dimension(200, 150));

        buttonPageEnd = new JButton("Page End");
        buttonLineStart = new JButton("Line Start");
        buttonLineEnd = new JButton("Line End");

        add(buttonPageStart, BorderLayout.PAGE_START);
        add(buttonPageEnd, BorderLayout.PAGE_END);
        add(buttonLineStart, BorderLayout.LINE_START);
        add(buttonLineEnd, BorderLayout.LINE_END);
        add(textAreaCenter, BorderLayout.CENTER);

        setSize(700, 500);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }
}
