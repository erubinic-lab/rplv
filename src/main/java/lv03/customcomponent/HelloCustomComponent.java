package lv03.customcomponent;

import javax.swing.*;
import java.awt.*;

public class HelloCustomComponent extends JFrame {
    private JButton button;
    private ScrollableTextArea scrollableTextArea;

    public HelloCustomComponent() {
        super("Custom component - Scrollable text area ");
        setLayout(new BorderLayout());

        scrollableTextArea = new ScrollableTextArea();
        button = new JButton("Append text");

        button.addActionListener(e -> {
            scrollableTextArea.appendToNewLine("Appending text");
        });

        add(button, BorderLayout.PAGE_START);
        add(scrollableTextArea, BorderLayout.CENTER);

        setSize(200, 200);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> new HelloCustomComponent());
    }
}
