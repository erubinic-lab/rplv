package lv03.customcomponent;


import javax.swing.*;
import java.awt.*;

public class ScrollableTextArea extends JPanel {
    private JTextArea textArea;

    public ScrollableTextArea() {
        textArea = new JTextArea();

        setLayout(new BorderLayout());

        //add(textArea);
        add(new JScrollPane(textArea), BorderLayout.CENTER);
    }

    public void appendToNewLine(String text) {
        textArea.append(text + "\n");
    }
}
