package lv03.multiactionlistener;

import javax.swing.*;

import java.awt.*;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MultiListener extends JPanel implements ActionListener {
    JTextArea multiListenerArea;
    JTextArea listenerArea1;
    JTextArea listenerArea2;
    JTextArea listenerArea3;
    JButton speakerButton1;
    JButton speakerButton2;
    JButton speakerButton3;

    public MultiListener() {
        super(new GridLayout(9, 1));

        JLabel l;

        JPanel p = new JPanel();
        speakerButton1 = new JButton("Speaker 1");
        p.add(speakerButton1);

        speakerButton2 = new JButton("Speaker 2");
        p.add(speakerButton2);

        speakerButton3 = new JButton("Speaker 3");
        p.add(speakerButton3);

        add(p);

        l = new JLabel("Listener 1:");
        add(l);
        listenerArea1 = new JTextArea();
        add(listenerArea1);

        l = new JLabel("Listener 2");
        add(l);
        listenerArea2 = new JTextArea();
        add(listenerArea2);

        l = new JLabel("Listener 3");
        add(l);
        listenerArea3 = new JTextArea();
        add(listenerArea3);

        l = new JLabel("Multi listener");
        add(l);
        multiListenerArea = new JTextArea();
        multiListenerArea.setEditable(false);
        JScrollPane multiAreaScrollPane = new JScrollPane(multiListenerArea);
        add(multiAreaScrollPane);


        speakerButton1.addActionListener(new SingleListener(listenerArea1));
        speakerButton2.addActionListener(new SingleListener(listenerArea2));
        speakerButton3.addActionListener(new SingleListener(listenerArea3));

        speakerButton1.addActionListener(this);
        speakerButton2.addActionListener(this);
        speakerButton3.addActionListener(this);

        setPreferredSize(new Dimension(300, 600));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        multiListenerArea.append(e.getActionCommand() + "\n");
        multiListenerArea.setCaretPosition(multiListenerArea.getDocument().getLength());
    }

    private void createAndShowGUI() {
        JFrame frame = new JFrame("Multi event listener");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JComponent newContentPane = new MultiListener();
        newContentPane.setOpaque(true); //content panes must be opaque
        frame.setContentPane(newContentPane);

        frame.pack();
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(() -> new MultiListener().createAndShowGUI());
    }
}

