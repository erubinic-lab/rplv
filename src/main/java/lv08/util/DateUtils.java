package lv08.util;

import java.time.LocalDate;

public class DateUtils {
    public static LocalDate today() {
        return LocalDate.now();
    }

    // intentionally skipped npe check
    public static LocalDate getEndOfMonth(LocalDate date) {
        return date.withDayOfMonth(date.lengthOfMonth());
    }

}
