package lv08.repo;

import lv08.model.results.Student;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface StudentRepository extends CrudRepository<Student, Integer> {
    Optional<Student> findByJmbag(String jmbag);
}
