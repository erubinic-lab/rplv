package lv08.repo;

import lv08.model.sales.Customer;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;

public interface CustomersRepository extends CrudRepository<Customer, Integer> {
    Collection<Customer> findAll();
}
