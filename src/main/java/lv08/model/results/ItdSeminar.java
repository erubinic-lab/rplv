package lv08.model.results;

import com.sun.istack.NotNull;
import lombok.Getter;

import javax.persistence.*;

@MappedSuperclass
@Getter
abstract class ItdSeminar {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int Id;

    private int studentId;

    private Integer presDesign;

    private Integer presContent;

    private Integer presLive;

    private Integer quality;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "studentId", insertable = false, updatable = false)
    Student student;

}
