package lv08.model.results;

import lombok.Getter;

import javax.persistence.*;

@Entity
@Getter
@Table(schema = "results", name = "rp_before_finals")
public class RpBeforeFinals {
    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int Id;

    private int activity;

    @Column(name = "dz_2")
    private int dz2;

    @Column(name = "dz_4")
    private int dz4;

    @Column(name = "dz_5")
    private int dz5;

    @Column(name = "dz_6")
    private int dz6;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "studentId", insertable = false, updatable = false)
    Student student;
}
