package lv08.model.results;

import com.sun.istack.NotNull;
import lombok.Getter;

import javax.persistence.*;

@Entity
@Getter
@Table(schema = "results", name = "student")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull
    private String jmbag;

    @OneToOne(fetch = FetchType.EAGER, mappedBy="student")
    private ItdFirstSeminar firstSeminar;

    @OneToOne(fetch = FetchType.EAGER, mappedBy="student")
    private ItdSecondSeminar secondSeminar;

    @OneToOne(fetch = FetchType.EAGER, mappedBy="student")
    private ItdActivity itdActivity;

    @OneToOne(fetch = FetchType.EAGER, mappedBy="student")
    private RpBeforeFinals rpBeforeFinals;

    @OneToOne(fetch = FetchType.EAGER, mappedBy="student")
    private RpFinals rpFinals;
}
