package lv08.model.results;

import lombok.Getter;

import javax.persistence.*;

@Entity
@Getter
@Table(schema = "results", name = "itd_second_seminar")
public class ItdSecondSeminar extends ItdSeminar {

    private Integer negativePoints;

}
