package lv08.model.results;

import lombok.Getter;

import javax.persistence.*;

@Entity
@Getter
@Table(schema = "results", name = "itd_first_seminar")
public class ItdFirstSeminar extends ItdSeminar {

    @Column(name = "Quantity")
    private Integer size;

    private Integer formatting;

}
