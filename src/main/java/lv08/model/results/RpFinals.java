package lv08.model.results;

import lombok.Getter;

import javax.persistence.*;

@Entity
@Getter
@Table(schema = "results", name = "rp_finals")
public class RpFinals {
    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int Id;

    private int studentId;

    private int presContent;

    private int presLive;

    private int report;

    @Column(name = "Bitbucket")
    private int svn;

    private int quality;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "studentId", insertable = false, updatable = false)
    Student student;
}
