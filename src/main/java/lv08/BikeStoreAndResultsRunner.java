package lv08;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class BikeStoreAndResultsRunner {
    public static void main(String[] args) {
        SpringApplication.run(BikeStoreAndResultsRunner.class, args);
    }
}
