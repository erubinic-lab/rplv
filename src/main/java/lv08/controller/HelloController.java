package lv08.controller;

import lombok.RequiredArgsConstructor;
import lv08.model.sales.Customer;
import lv08.service.CustomerService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class HelloController {
    private final CustomerService customerService;

    @RequestMapping("/")
    public String hello() {
        return "Hello RPLV!";
    }

    @RequestMapping("/customers/all")
    public List<Customer> get() {
        return customerService.getAllCustomers();
    }
}


