package lv08.controller;

import lombok.RequiredArgsConstructor;
import lv08.service.ItdResultsService;
import lv08.service.RpResultsService;
import lv08.service.model.ItdResultResponse;
import lv08.service.model.RpResultResponse;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/results")
public class ResultsController {
    private final ItdResultsService itdResultsService;
    private final RpResultsService rpResultsService;

    @RequestMapping(value = "/itd/{jmbag}")
    ItdResultResponse getItdResults(@PathVariable("jmbag")  String jmbag) {
        return itdResultsService.getItdResult(jmbag);
    }

    @RequestMapping(value = "/rp/{jmbag}")
    RpResultResponse getRpResults(@PathVariable("jmbag")  String jmbag) {
        return rpResultsService.getRpResult(jmbag);
    }
}


