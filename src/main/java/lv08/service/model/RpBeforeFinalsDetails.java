package lv08.service.model;

import lombok.Builder;
import lombok.Getter;
import lv08.model.results.ItdFirstSeminar;
import lv08.model.results.RpBeforeFinals;

@Builder
@Getter
public class RpBeforeFinalsDetails {
    private Integer activity;
    private Integer dz2;
    private Integer dz4;
    private Integer dz5;
    private Integer dz6;

    public static RpBeforeFinalsDetails fromRpBeforeFinals(RpBeforeFinals req) {
        return RpBeforeFinalsDetails.builder()
                .activity(req.getActivity())
                .dz2(req.getDz2())
                .dz4(req.getDz4())
                .dz5(req.getDz5())
                .dz6(req.getDz6())
                .build();
    }

    public int calculateResult() {
        return activity + dz2 + dz4 + dz5 + dz6;
    }

}
