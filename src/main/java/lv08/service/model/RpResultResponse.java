package lv08.service.model;

import lombok.Data;

@Data
public class RpResultResponse {
    String jmbag;
    RpBeforeFinalsDetails rpBeforeFinalsDetails;
    RpFinalsDetails rpFinalsDetails;
    RpFinalResults finalResults;

    String error;
}
