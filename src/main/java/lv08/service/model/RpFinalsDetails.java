package lv08.service.model;

import lombok.Builder;
import lombok.Getter;
import lv08.model.results.RpFinals;

@Builder
@Getter
public class RpFinalsDetails {
    private static final int MAX_ACTUAL_POINTS = 70;
    private static final int TEACHER_PRES_MAX_RATING = 20;

    // sum of bellow metrics bellow should be 100
    private static final int PRES_CONTENT_PERCENTAGE = 10;
    private static final int PRES_LIVE_PERCENTAGE = 15;
    private static final int REPORT_PERCENTAGE = 20;
    private static final int SVN_PERCENTAGE = 15;
    private static final int QUALITY_PERCENTAGE = 40;

    private Integer presContent;
    private Integer presLive;
    private Integer report;
    private Integer svn;
    private Integer quality;

    public static RpFinalsDetails fromRpFinals(RpFinals req) {
        if (req == null)
            return RpFinalsDetails.builder().build(); // only because all students didn't show up on final exam

        return RpFinalsDetails.builder()
                .presContent(req.getPresContent())
                .presLive(req.getPresLive())
                .report(req.getReport())
                .svn(req.getSvn())
                .quality(req.getQuality())
                .build();
    }

    public Integer calculateResult() {
        if (!isValid())
            throw new RuntimeException("Invalid scores for finals.");

        float points = calculateOneMetricPoint(presContent, PRES_CONTENT_PERCENTAGE) +
                calculateOneMetricPoint(presLive, PRES_LIVE_PERCENTAGE) +
                calculateOneMetricPoint(report, REPORT_PERCENTAGE) +
                calculateOneMetricPoint(svn, SVN_PERCENTAGE) +
                calculateOneMetricPoint(quality, QUALITY_PERCENTAGE);

        points = (points / 100F) * MAX_ACTUAL_POINTS;

        return Math.round(points);
    }

    private float calculateOneMetricPoint(Integer value, int percentage) {
        if (value == null)
            value = 0;

        return ((float) value / TEACHER_PRES_MAX_RATING) * percentage;
    }

    public boolean isValid() {
        if (presContent == null)
            return false;
        if (presLive == null)
            return false;
        if (report == null)
            return false;
        if (svn == null)
            return false;
        if (quality == null)
            return false;

        return true;
    }

}
