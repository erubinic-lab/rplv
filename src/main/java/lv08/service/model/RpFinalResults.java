package lv08.service.model;

import lombok.Builder;
import lombok.Data;
import lv08.enums.Grade;

@Data
@Builder(toBuilder = true)
public class RpFinalResults {
    Integer beforeFinals;
    Integer finals;
    Integer total;
    Grade grade;

    public static RpFinalResults fromRpResultResponse(RpResultResponse rpResponse) {
        return RpFinalResults.builder()
                .beforeFinals(rpResponse.getRpBeforeFinalsDetails().calculateResult())
                // only because all students didn't show up on final exam
                .finals(rpResponse.getRpFinalsDetails().isValid() ? rpResponse.getRpFinalsDetails().calculateResult() : null)
                .build()
                .addTotalAndGrade();
    }

    private RpFinalResults addTotalAndGrade() {
        if (finals == null)
            return this;

        int minimumForScaling = 40;

        int tmpTotal = beforeFinals + finals;

        if (tmpTotal < minimumForScaling)
            tmpTotal = minimumForScaling;

        total = tmpTotal;
        grade = Grade.fromPoints(total);

        return this;
    }
}
