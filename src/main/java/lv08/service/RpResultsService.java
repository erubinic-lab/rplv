package lv08.service;

import lv08.service.model.RpResultResponse;

public interface RpResultsService {

    RpResultResponse getRpResult(String jmbag);

}
