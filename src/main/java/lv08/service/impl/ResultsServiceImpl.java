package lv08.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lv08.model.results.ItdActivity;
import lv08.model.results.Student;
import lv08.repo.StudentRepository;
import lv08.service.ItdResultsService;
import lv08.service.RpResultsService;
import lv08.service.model.*;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class ResultsServiceImpl implements ItdResultsService, RpResultsService {
    private final StudentRepository studentRepository;

    @Override
    public ItdResultResponse getItdResult(String jmbag) {
        ItdResultResponse response = new ItdResultResponse();

        try {
            Student student = getStudent(jmbag).get();

            response.setJmbag(student.getJmbag());
            response.setFirstSeminarDetails(
                    ItdSeminarOneDetails.fromItdSeminar(student.getFirstSeminar()));
            response.setSecondSeminarDetails(
                    ItdSeminarTwoDetails.fromItdSeminar(student.getSecondSeminar()));
            response.setActivity(resolveActivity(student));
            response.setFinalResults(ItdFinalResults.fromItdFinalResponse(response));
        } catch (Exception ex) {
            log.error("Error getting ITD results for jmbag:{}; ", jmbag, ex);
            response.setError(ex.getMessage());
        }

        return response;
    }

    @Override
    public RpResultResponse getRpResult(String jmbag) {
        RpResultResponse response = new RpResultResponse();

        try {
            Student student = getStudent(jmbag).get();

            response.setJmbag(student.getJmbag());
            response.setRpBeforeFinalsDetails(
                    RpBeforeFinalsDetails.fromRpBeforeFinals(student.getRpBeforeFinals()));
            response.setRpFinalsDetails(RpFinalsDetails.fromRpFinals(student.getRpFinals()));

            if(!response.getRpFinalsDetails().isValid())
                response.setError("Empty final exam results");

            response.setFinalResults(RpFinalResults.fromRpResultResponse(response));
        } catch (Exception ex) {
            log.error("Error getting RP results for jmbag:{}; ", jmbag, ex);
            response.setError(ex.getMessage());
        }

        return response;
    }

    private Optional<Student> getStudent(String jmbag) {
        return Optional.of(studentRepository.findByJmbag(jmbag))
                .orElse(Optional.empty());
    }

    private Integer resolveActivity(Student student) {
        return Optional.ofNullable(student.getItdActivity())
                .map(ItdActivity::getActivity)
                .orElse(null);
    }

}
