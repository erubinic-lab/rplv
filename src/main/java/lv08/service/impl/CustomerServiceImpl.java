package lv08.service.impl;

import lombok.RequiredArgsConstructor;
import lv08.model.sales.Customer;
import lv08.repo.CustomersRepository;
import lv08.service.CustomerService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CustomerServiceImpl implements CustomerService {
    private final CustomersRepository customersRepository;

    @Override
    public List<Customer> getAllCustomers() {
        return customersRepository.findAll()
                .stream()
                .toList();
    }

    @Override
    public Optional<Customer> getCustomerById(int customerId) {
        return Optional.of(customersRepository.findById(customerId))
                .orElse(Optional.empty());
    }
}
