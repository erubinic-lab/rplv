package lv08.service;

import lv08.service.model.ItdResultResponse;

public interface ItdResultsService {

    ItdResultResponse getItdResult(String jmbag);

}
