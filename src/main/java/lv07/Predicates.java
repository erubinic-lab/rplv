package lv07;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Predicate;

public class Predicates {
    public static void main(String[] args) {
        Logger logger = LoggerFactory.getLogger("Logger");

        Predicate<Integer> lessThan = i -> (i < 1000);

        logger.info("{}", lessThan.test(10));

        Predicate<Integer> greaterThan = i -> (i > 500);

        logger.info("{}", lessThan.and(greaterThan).test(18));
        logger.info("{}", lessThan.and(greaterThan).test(501));
    }

}
