package lv07;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class PrintIntList {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(1, 2, 3, 1, 5, 8, 10 ,8, 8, 1, 2);

        for(int number : numbers){
            System.out.println(number);
        }

        numbers.forEach(number -> System.out.println(number));


        Consumer<Integer> printNumbers = number -> System.out.println(number);

        numbers.forEach(printNumbers);
    }
}
