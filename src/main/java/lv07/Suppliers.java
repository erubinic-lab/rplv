package lv07;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Supplier;

public class Suppliers {
    public static void main(String[] args) {
        Logger logger = LoggerFactory.getLogger("logger");

        Supplier<String> stringSuppliers = () ->  "Hello RPLV";

        String someText = stringSuppliers.get();

        logger.info("{}", someText);
    }
}
