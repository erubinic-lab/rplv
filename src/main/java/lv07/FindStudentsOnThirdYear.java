package lv07;

import lv06.helloLombok.StudentLombok;

import java.util.*;
import java.util.stream.Collectors;

public class FindStudentsOnThirdYear {
    public static void main(String[] args) {
        List<StudentLombok> thirdYearStudents = StudentData.getAllStudents().stream()
                .filter(Objects::nonNull)
                .filter(it -> it.getYearOfStudy() == 3)
                .collect(Collectors.toList());

        List<String> thirdYearStudentNames = StudentData.getAllStudents().stream()
                .filter(Objects::nonNull)
                .filter(it -> it.getYearOfStudy() == 3)
                .map(StudentLombok::getName)
                .collect(Collectors.toList());

        // or

        List<String> thirdYearStudentNames2 = thirdYearStudents.stream()
                .map(StudentLombok::getName)
                .collect(Collectors.toList());

        // or

        Set<String> thirdYearStudentsNames3 = getDistinctStudentNames(StudentData.getAllStudents());
    }

    public static Set<String> getDistinctStudentNames(List<StudentLombok> students){
        return Optional.ofNullable(students)
                .orElse(Collections.emptyList())
                .stream()
                .filter(Objects::nonNull)
                .filter(it -> it.getYearOfStudy() == 3)
                .map(StudentLombok::getName)
                .collect(Collectors.toSet());
    }
}
