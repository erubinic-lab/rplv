package lv07;

import lombok.Getter;
import lv06.helloLombok.StudentLombok;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

public class StudentData {
    @Getter
    private static List<StudentLombok> allStudents = Arrays.asList(
            new StudentLombok("Pero", 2 , BigDecimal.valueOf(4), 1990, "095949494"),
            new StudentLombok("Marko", 3 , BigDecimal.valueOf(3), 1980, "856756"),
            null,
            new StudentLombok("Ana", 1 , BigDecimal.valueOf(4), 1980, "095949494"),
            new StudentLombok("Mara", 2 , BigDecimal.valueOf(5), 1980, "1225426"),
            new StudentLombok("Pera", 2 , BigDecimal.valueOf(4), 1980, "095987566849494"),
            new StudentLombok("Ara", 2 , BigDecimal.valueOf(5), 1990, "09594786879494"),
            new StudentLombok("Dadra", 3 , BigDecimal.valueOf(4), 2000, "7856"),
            new StudentLombok("Mara", 2 , BigDecimal.valueOf(4), 1990, "87567"),
            new StudentLombok("Marko", 2 , BigDecimal.valueOf(1), 1990, "095949494"),
            new StudentLombok("Ana", 3 , BigDecimal.valueOf(1), 2000, "095949494"),
            new StudentLombok("Iva", 2 , BigDecimal.valueOf(1), 1990, "095949494"),
            new StudentLombok("Pero", 2 , BigDecimal.valueOf(4), 2000, "87567"),
            new StudentLombok("Ana", 3 , BigDecimal.valueOf(8), 1990, "095949494"),
            new StudentLombok("Marko", 2 , BigDecimal.valueOf(4), 2001, "087695949494"),
            new StudentLombok("Pero", 2 , BigDecimal.valueOf(3), 1990, "095949494"),
            new StudentLombok("Niko", 5 , BigDecimal.valueOf(4), 1990, "095949494"),
            new StudentLombok("Pero", 2 , BigDecimal.valueOf(2), 1990, "095949494"),
            new StudentLombok("Ivan", 2 , BigDecimal.valueOf(4), 2010, "095949494"),
            null,
            new StudentLombok("Pero", 2 , BigDecimal.valueOf(4), 2010, "095949494"),
            new StudentLombok("Pero", 5 , BigDecimal.valueOf(5), 1990, "878756876"),
            new StudentLombok("Pero", 2 , BigDecimal.valueOf(4), 1990, "095949494"),
            new StudentLombok("Pero", 2 , BigDecimal.valueOf(4), 2010, "095949494"),
            new StudentLombok("Pero", 6 , BigDecimal.valueOf(6), 1990, "0956786949494"),
            new StudentLombok("Pero", 2 , BigDecimal.valueOf(4), 1990, "876"),
            new StudentLombok("Pero", 2 , BigDecimal.valueOf(7), 1890, "095949494"),
            new StudentLombok("Pero",  5, BigDecimal.valueOf(4), 1990, "095949494"),
            new StudentLombok("Pero", 2 , BigDecimal.valueOf(8), 1990, "786876"),
            new StudentLombok("Pero", 2 , BigDecimal.valueOf(4), 1990, "7856"),
            new StudentLombok("Marko", 3 , BigDecimal.valueOf(4), 1990, "095949494"),
            new StudentLombok("Pero", 2 , BigDecimal.valueOf(4), 1990, "87566")
    );
}
