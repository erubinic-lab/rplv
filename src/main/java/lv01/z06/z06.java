package lv01.z06;

public class z06 {
    // dinamicki polimorfizam - "method overriding"
    // BadStudent nasljeduje metodu learn() od parent klase Student. Zbog toga sto BadStudent ima svoju implementaciju
    // metode learn() kod poziva badStudent#learn() pozvati ce se "override-ana" implementacija
    public static void main(String[] args) {
        Student student = new Student();
        student.learn();

        BadStudent badStudent = new BadStudent();
        badStudent.learn();
    }
}
