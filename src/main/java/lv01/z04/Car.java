package lv01.z04;

public class Car extends Vehicle {

    private int wheelNumber = 4;
    private int doorNumber = 5;

    public Car(String engineType, String color) {
        super(engineType, color);
    }

    public int getWheelNumber() {
        return wheelNumber;
    }

    public int getDoorNumber() {
        return doorNumber;
    }

    // nasljedivanje - klasa Car nasljeduje metode klase Vehicle. Klasa Car nasljeduje sva polja i metode klase Vehicle
    // te implementira dodatne metode
    public static void main(String[] args) {
        Car car = new Car("benzin", "plava");

        System.out.println("Broj guma: " + car.getWheelNumber());
        System.out.println("Broj vrata: " + car.getDoorNumber());
        car.printEngineType(); // Vehicle
        car.printColor(); ; // Vehicle
    }
}
