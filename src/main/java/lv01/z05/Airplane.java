package lv01.z05;

public class Airplane {

    void fly() {
        System.out.println("Avion leti");
    }

    void fly(int height) {
        System.out.println("Avion leti na visini " + height + " metara");
    }

    void fly(int length, String planeName) {
        System.out.println("Najduži let avionom bio je " + length + " kilometara, a let je odvozio avion naziva " + planeName);
    }

    // staticki polimorfizam - "method overloading" - klasa Airplane sadrzi tri metode s istim imenom, ali s razlicitim parametrima
    public static void main(String[] args) {
        Airplane airplane = new Airplane();

        airplane.fly();
        airplane.fly(8_000);
        airplane.fly(10_000, "Perolet");
    }
}
