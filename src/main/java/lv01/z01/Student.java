package lv01.z01;

abstract class Student {

    public abstract void learn();

    public abstract void sleep();

    public void intro() {
        System.out.println("Studentski zivot");
    }
}
