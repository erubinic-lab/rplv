package lv01.z01;

public class z01 {
    // primjer apstrakcije
    public static void main(String[] args) {
        // Student student = new Student(); // error - ne moze se inicijalizirati instanca apstraktne klase

        GoodStudent goodStudent = new GoodStudent(); // moze se inicijalizirati child klasa, koja moze imati implementacije abstraktne klase te vlastite
        BadStudent badStudent = new BadStudent();

        goodStudent.intro();
        goodStudent.learn();
        goodStudent.sleep();

        badStudent.intro();
        badStudent.learn();
        badStudent.sleep();
    }
}
