package lv01.z01;

public class GoodStudent extends Student {
    @Override
    public void learn() {
        System.out.println("Dobar student uci 10 sati dnevno");
    }

    @Override
    public void sleep() {
        System.out.println("Dobar student spava 3 sati dnevno");
    }
}
