package lv01.z01;

public class BadStudent extends Student {
    @Override
    public void learn() {
        System.out.println("Los student uci 0 sati dnevno");
    }

    @Override
    public void sleep() {
        System.out.println("Los student spava 10 sati dnevno");
    }
}
