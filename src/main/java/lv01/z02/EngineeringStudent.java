package lv01.z02;

public class EngineeringStudent implements Student, Human {
    @Override
    public void printYearAndCollegeName() {
        System.out.println("Godina studija: " + year);
        System.out.println("Ime fakulteta: " + collegeName);
    }

    @Override
    public void watchTv() {
        System.out.println("Ne gleda televiziju");
    }

    @Override
    public void sleep() {
        System.out.println("Spava 8h");
    }

    // interface je 100% apstraktna klasa. Moze sadrzavati jedino static, final, public polja te apstraktne metode
    public static void main(String[] args) {
        EngineeringStudent engineeringStudent = new EngineeringStudent();

        engineeringStudent.watchTv();
        engineeringStudent.sleep();
        engineeringStudent.printYearAndCollegeName();
    }
}
