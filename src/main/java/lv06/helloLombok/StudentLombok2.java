package lv06.helloLombok;


import lombok.*;

import java.math.BigDecimal;

@AllArgsConstructor
@RequiredArgsConstructor
@ToString
public class StudentLombok2 {
    @Getter
    private final String name;
    private final Integer yearOfStudy;
    private final BigDecimal averageGrade;
    @Setter
    private Integer birthYear;
    private String phoneNumber;

    public static void main(String[] args) {
        StudentLombok2 student = new StudentLombok2("Pero", 4, BigDecimal.valueOf(5));

        System.out.println(student);

        student = new StudentLombok2("Pero", 4, BigDecimal.valueOf(5), 1992, "095949494");

        System.out.println(student.getName());

        student.setBirthYear(1800);

        System.out.println(student);

    }
}
