package lv06.guavacache;


import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import java.util.concurrent.TimeUnit;

public class LoadingCacheExample {
    LoadingCache<Integer, Customer> cache;

    public void Initialize(){
        CacheLoader<Integer, Customer> cacheLoader = new CacheLoader<Integer, Customer>() {
            @Override
            public Customer load(Integer integer) throws Exception {
                // write me
                return null;
            }
        };

        cache = CacheBuilder.newBuilder()
                .maximumSize(100)
                .expireAfterWrite(1, TimeUnit.MINUTES)
                .build(cacheLoader);
    }

}
