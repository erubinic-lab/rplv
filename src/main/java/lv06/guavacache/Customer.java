package lv06.guavacache;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Customer {
    int id;
    String firstName;
    String lastName;
    String phone;
    String email;
    String city;
    String state;
    String zipcode;
}
