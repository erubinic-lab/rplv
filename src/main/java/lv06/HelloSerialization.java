package lv06;


import com.fasterxml.jackson.databind.ObjectMapper;
import lv06.helloLombok.StudentLombok;
import lv06.hellobuilder.Student;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;

public class HelloSerialization {
    public static void main(String[] args) {
        ObjectMapper mapper = new ObjectMapper();

        StudentLombok student = StudentLombok.builder()
                .name("Pero")
                .yearOfStudy(3)
                .averageGrade(BigDecimal.ONE)
                .birthYear(1990)
                .phoneNumber("095949494")
                .build();

        try {
            File directory = new File("jackson");
            if (!directory.exists())
                directory.mkdir();

            mapper.writeValue(new File("jackson/studentPero.json"), student);

            // byte[] studentBytes = mapper.writeValueAsBytes(student);

            StudentLombok studentPeroFromJsonFile = mapper.readValue(new File("jackson/studentPero.json"),
                    StudentLombok.class);
            System.out.println(studentPeroFromJsonFile);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
