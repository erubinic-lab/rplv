package lv02.helloswing;

import javax.swing.*;

public class HelloSwingV2 {

    public static void main(String[] args) {
        // this is lambda expression for wingUtilities.invokeLater(new Runnable() { public void run() {})
        SwingUtilities.invokeLater(() -> {
            JFrame frame = new JFrame("Hello Swing");
            JLabel label = new JLabel("First label using Swing");

            label.setBounds(100, 200, 200, 30);
            frame.add(label);
            frame.setSize(700, 500);
            frame.setLayout(null); // null layout means absolute positioning. Not using layout manager
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        });
    }
}
