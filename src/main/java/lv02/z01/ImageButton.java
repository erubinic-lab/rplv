package lv02.z01;

import javax.swing.*;
import java.net.URL;


public class ImageButton {
    JFrame frame;
    JButton button;

    ImageButton() {
        frame = new JFrame("Hello Swing");


        String greenYellowPath = "img_green_yellow.png";
        String yellowGreenPath = "img_yellow_green.png";

        checkResourceExist(greenYellowPath);
        checkResourceExist(yellowGreenPath);

        // If " Cannot invoke "java.net.URL.toExternalForm()" because "location" is null"
        // or "resource not found!"
        // Need to set project resources root directory:
        //     File - Project Structure - Module - Resources
        // or
        //     Resource names should not be capitalized
        button = new JButton("Click me");
        button.setHorizontalTextPosition(JButton.CENTER);
        button.setVerticalTextPosition(JButton.CENTER);
        button.setIcon(new ImageIcon(getClass().getClassLoader().getResource(greenYellowPath)));
        button.setRolloverIcon(new ImageIcon(getClass().getClassLoader().getResource(yellowGreenPath)));
        button.setBounds(100, 100, 100, 100);

        frame.add(button);
        frame.setSize(700, 500);
        frame.setLayout(null); // null layout means absolute positioning. Not using layout manager
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

    private void checkResourceExist(String path) {
        URL resource = getClass().getClassLoader().getResource(path);
        if (resource == null)
            throw new IllegalArgumentException("Resource not found! " + path);
    }

    public void show() {
        frame.setVisible(true);
    }

    public static void main(String[] args) {

        SwingUtilities.invokeLater(() -> {
            new ImageButton().show();
        });
    }
}
