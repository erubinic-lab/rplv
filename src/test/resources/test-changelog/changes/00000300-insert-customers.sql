--liquibase formatted sql

--changeset erubnic:1 logicalFilePath:changelogs/00000100-insert-customers.sql

INSERT INTO sales.customers(first_name, last_name, phone, email, street, city, state, zip_code) VALUES
('Pero','Peric','095095905','pero@yahoo.com','Periceva 65 ','Rijeka','pgz',51000),
('Ana','Anic',NULL,'ana@yahoo.com','Anina 83 ','Zagreb','zg',10000),
('Marko','Markic',NULL,'marko@aol.com','Ulica sv. Marka 2','Rijeka','pgz',51000);