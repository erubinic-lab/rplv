package it.lv08;

import lv08.BikeStoreAndResultsRunner;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = BikeStoreAndResultsRunner.class)
@Import(TestConfig.class)
@ActiveProfiles("test")
public abstract class TestHelper {

}
