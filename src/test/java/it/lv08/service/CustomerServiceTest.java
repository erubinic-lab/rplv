package it.lv08.service;

import it.lv08.TestHelper;
import lv08.model.sales.Customer;
import lv08.service.CustomerService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class CustomerServiceTest extends TestHelper {
    @Autowired
    private CustomerService customerService;

    @Test
    public void helloTest(){
        Customer customerExpected = new Customer();
            customerExpected.setId(1);
            customerExpected.setFirstName("Pero");
            customerExpected.setLastName("Peric");
        customerExpected.setPhone("095095905");
        customerExpected.setEmail("pero@yahoo.com");

        Optional<Customer> customer = customerService.getCustomerById(1);

        assertTrue(customer.isPresent());
        assertEquals(customerExpected, customer.get());
    }
}