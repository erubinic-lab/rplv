package lv08.util;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class DateUtilsTest {
    @Test
    public void getLastDayOfMonth_firstDayOnMonth() {
        LocalDate now = LocalDate.of(2021, 2, 1);

        LocalDate eom = DateUtils.getEndOfMonth(now);

        assertEquals(LocalDate.of(2021, 2, 28), eom);
    }

    @Test
    public void getLastDayOfMonth_middleDayOfMonth() {
        LocalDate now = LocalDate.of(2021, 11, 15);

        LocalDate eom = DateUtils.getEndOfMonth(now);

        assertEquals(LocalDate.of(2021, 11, 30), eom);
    }

    @Test
    public void getLastDayOfMonth_lastDayOfMonth() {
        LocalDate now = LocalDate.of(2021, 12, 31);

        LocalDate eom = DateUtils.getEndOfMonth(now);

        assertEquals(now, eom);
    }

    @Test
    public void getLastDayOfMonth_npe() {
        Exception exception = assertThrows(NullPointerException.class, () -> DateUtils.getEndOfMonth(null));

        String expectedMessage = "Cannot invoke \"java.time.LocalDate.lengthOfMonth()\" because \"date\" is null";

        assertEquals(expectedMessage, exception.getMessage());
    }
}
